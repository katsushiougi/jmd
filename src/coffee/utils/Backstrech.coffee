define () ->

  class Backstrech

    ###
    @private
    ###
    $window = $ window
    ratio   = 0
    minW    = null
    minH    = null

    ###
    コンストラクタ
    ###
    constructor: (@$el) ->
      $img = @$el.find "img"
      # 比率設定
      ratio = $img.width() / $img.height()

    ###
    全画面にリサイズする処理
    ###
    resize: ->
      bgCSS =
        left:0
        top :0
      rootWidth  = $window.width()
      bgWidth    = rootWidth
      rootHeight = $window.height()
      bgHeight   = bgWidth / ratio #画像の実寸
      bgOffset   = 0

      if bgHeight >= rootHeight
        # 画像の高さがウインドウの高さより大きい場合
        bgOffset  = (bgHeight - rootHeight) / 2
        bgCSS.top = "-" + bgOffset + "px"
      else
        # 画像の高さがウインドウの高さより小さい場合
        bgHeight   = rootHeight
        bgWidth    = bgHeight * ratio
        bgOffset   = (bgWidth - rootWidth) / 2
        bgCSS.left = '-' + bgOffset + 'px'

      @$el.css
        width : rootWidth
        height: rootHeight
      .find("img").css
        width : bgWidth
        height: bgHeight
      .css(bgCSS)

  ###
  Exports
  ###
  return Backstrech