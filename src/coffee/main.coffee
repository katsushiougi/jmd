require [
  "utils/Backstrech"
], (
  Backstretch
) ->

  wait = (time) ->
    $.Deferred (defer) ->
      setTimeout ->
        defer.resolve()
      , time

  # 背景全画面
  backstrech = new Backstretch $(".js-backstrech")
  backstrech.resize()

  $(window).on "resize", () -> backstrech.resize()

  # コンテンツ設定
  height = 0
  for blk, i in $("#contents").find(".mod-blk")
    h = $(blk).height()
    height += h
    $(blk).css
      top: i * h

  # ナビゲーション
  $("#gnav li a").on "click", (e) =>
    e.preventDefault()
    $me = $ e.currentTarget
    target = $me[0].hash
    nextY = $(target).offset().top
    $("#contents").find(".mod-blk").each (i, el) ->
      targetY = $(el).offset().top - nextY
      $(el).addClass "on"
      TweenMax.to el, .8,
        scale: .5
        ease: Quad.easeIn
        onComplete: ->
      TweenMax.to el, .8,
        top: targetY
        ease: Quad.easeIn
        onComplete: ->
      wait(500).done ->
        TweenMax.to el, .8,
          scale: 1
          ease: Quad.easeOut
