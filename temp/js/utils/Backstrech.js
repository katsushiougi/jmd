(function() {
  define(function() {
    var Backstrech;
    Backstrech = (function() {
      /*
      @private
      */

      var $window, minH, minW, ratio;

      $window = $(window);

      ratio = 0;

      minW = null;

      minH = null;

      /*
      コンストラクタ
      */


      function Backstrech($el) {
        var $img;
        this.$el = $el;
        $img = this.$el.find("img");
        ratio = $img.width() / $img.height();
      }

      /*
      全画面にリサイズする処理
      */


      Backstrech.prototype.resize = function() {
        var bgCSS, bgHeight, bgOffset, bgWidth, rootHeight, rootWidth;
        bgCSS = {
          left: 0,
          top: 0
        };
        rootWidth = $window.width();
        bgWidth = rootWidth;
        rootHeight = $window.height();
        bgHeight = bgWidth / ratio;
        bgOffset = 0;
        if (bgHeight >= rootHeight) {
          bgOffset = (bgHeight - rootHeight) / 2;
          bgCSS.top = "-" + bgOffset + "px";
        } else {
          bgHeight = rootHeight;
          bgWidth = bgHeight * ratio;
          bgOffset = (bgWidth - rootWidth) / 2;
          bgCSS.left = '-' + bgOffset + 'px';
        }
        return this.$el.css({
          width: rootWidth,
          height: rootHeight
        }).find("img").css({
          width: bgWidth,
          height: bgHeight
        }).css(bgCSS);
      };

      return Backstrech;

    })();
    /*
    Exports
    */

    return Backstrech;
  });

}).call(this);
