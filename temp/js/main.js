(function() {
  require(["utils/Backstrech"], function(Backstretch) {
    var backstrech, blk, h, height, i, wait, _i, _len, _ref,
      _this = this;
    wait = function(time) {
      return $.Deferred(function(defer) {
        return setTimeout(function() {
          return defer.resolve();
        }, time);
      });
    };
    backstrech = new Backstretch($(".js-backstrech"));
    backstrech.resize();
    $(window).on("resize", function() {
      return backstrech.resize();
    });
    height = 0;
    _ref = $("#contents").find(".mod-blk");
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      blk = _ref[i];
      h = $(blk).height();
      height += h;
      $(blk).css({
        top: i * h
      });
    }
    return $("#gnav li a").on("click", function(e) {
      var $me, nextY, target;
      e.preventDefault();
      $me = $(e.currentTarget);
      target = $me[0].hash;
      nextY = $(target).offset().top;
      return $("#contents").find(".mod-blk").each(function(i, el) {
        var targetY;
        targetY = $(el).offset().top - nextY;
        $(el).addClass("on");
        TweenMax.to(el, .8, {
          scale: .5,
          ease: Quad.easeIn,
          onComplete: function() {}
        });
        TweenMax.to(el, .8, {
          top: targetY,
          ease: Quad.easeIn,
          onComplete: function() {}
        });
        return wait(500).done(function() {
          return TweenMax.to(el, .8, {
            scale: 1,
            ease: Quad.easeOut
          });
        });
      });
    });
  });

}).call(this);
