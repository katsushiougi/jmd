// Wed, 04 Dec 2013 08:18:23 GMT
(function() {
(function() {
  define('utils/Backstrech',[],function() {
    var Backstrech;
    Backstrech = (function() {
      /*
      @private
      */

      var $window, minH, minW, ratio;

      $window = $(window);

      ratio = 0;

      minW = null;

      minH = null;

      /*
      コンストラクタ
      */


      function Backstrech($el) {
        var $img;
        this.$el = $el;
        $img = this.$el.find("img");
        ratio = $img.width() / $img.height();
      }

      /*
      全画面にリサイズする処理
      */


      Backstrech.prototype.resize = function() {
        var bgCSS, bgHeight, bgOffset, bgWidth, rootHeight, rootWidth;
        bgCSS = {
          left: 0,
          top: 0
        };
        rootWidth = $window.width();
        bgWidth = rootWidth;
        rootHeight = $window.height();
        bgHeight = bgWidth / ratio;
        bgOffset = 0;
        if (bgHeight >= rootHeight) {
          bgOffset = (bgHeight - rootHeight) / 2;
          bgCSS.top = "-" + bgOffset + "px";
        } else {
          bgHeight = rootHeight;
          bgWidth = bgHeight * ratio;
          bgOffset = (bgWidth - rootWidth) / 2;
          bgCSS.left = '-' + bgOffset + 'px';
        }
        return this.$el.css({
          width: rootWidth,
          height: rootHeight
        }).find("img").css({
          width: bgWidth,
          height: bgHeight
        }).css(bgCSS);
      };

      return Backstrech;

    })();
    /*
    Exports
    */

    return Backstrech;
  });

}).call(this);

(function() {
  require(["utils/Backstrech"], function(Backstretch) {
    var backstrech, blk, h, height, i, wait, _i, _len, _ref,
      _this = this;
    wait = function(time) {
      return $.Deferred(function(defer) {
        return setTimeout(function() {
          return defer.resolve();
        }, time);
      });
    };
    backstrech = new Backstretch($(".js-backstrech"));
    backstrech.resize();
    $(window).on("resize", function() {
      return backstrech.resize();
    });
    height = 0;
    _ref = $("#contents").find(".mod-blk");
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      blk = _ref[i];
      h = $(blk).height();
      height += h;
      $(blk).css({
        top: i * h
      });
    }
    return $("#gnav li a").on("click", function(e) {
      var $me, nextY, target;
      e.preventDefault();
      $me = $(e.currentTarget);
      target = $me[0].hash;
      nextY = $(target).offset().top;
      return $("#contents").find(".mod-blk").each(function(i, el) {
        var targetY;
        targetY = $(el).offset().top - nextY;
        $(el).addClass("on");
        TweenMax.to(el, .8, {
          scale: .5,
          ease: Quad.easeIn,
          onComplete: function() {}
        });
        TweenMax.to(el, .8, {
          top: targetY,
          ease: Quad.easeIn,
          onComplete: function() {}
        });
        return wait(500).done(function() {
          return TweenMax.to(el, .8, {
            scale: 1,
            ease: Quad.easeOut
          });
        });
      });
    });
  });

}).call(this);

define("main", function(){});
} ());